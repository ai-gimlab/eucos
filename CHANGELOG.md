# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.0.0](https://gitlab.com/ai-gimlab/eucos/-/releases/v1.0.0) - 2024-07-21

### First stable release

---

[Back to project main page](https://gitlab.com/ai-gimlab/eucos#ieucos)

---
