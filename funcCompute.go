/*  'eucos' Euclidean Distance or Cosine Similarity using CSV files as vectors embedding DBs
    Copyright (C) 2024 gimlab

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package main

import (
	"errors"
	"math"
	"strconv"
	"strings"
	"unicode"
)

// stringToVector convert embedding array of strings to an array of floats
func stringToVector(records [][]string) (v [][]float64, err error) {
	var vectors [][]float64 = make([][]float64, len(records), len(records))
	for i := 0; i < len(records); i++ {
		embeddingStrings := records[i][2]
		// remove square brackets
		trimmed := strings.TrimFunc(embeddingStrings, func(r rune) bool {
			return !unicode.IsNumber(r)
		})
		r := strings.Split(trimmed, ",")
		vec := make([]float64, len(r), len(r))
		for v := 0; v < len(r); v++ {
			sanitized := strings.Trim(r[v], " ")
			f, err := strconv.ParseFloat(sanitized, 64)
			if err != nil {
				return nil, err
			}
			vec[v] = f
		}
		vectors[i] = vec
	}
	return vectors, nil
}

// Cosine compute cosine similarities
func Cosine(a, b []float64) (cosine float64, err error) {
	count := 0
	length_a := len(a)
	length_b := len(b)
	if length_a > length_b {
		count = length_a
	} else {
		count = length_b
	}
	sumA := 0.0
	s1 := 0.0
	s2 := 0.0
	for k := 0; k < count; k++ {
		if k >= length_a {
			s2 += math.Pow(b[k], 2)
			continue
		}
		if k >= length_b {
			s1 += math.Pow(a[k], 2)
			continue
		}
		sumA += a[k] * b[k]
		s1 += math.Pow(a[k], 2)
		s2 += math.Pow(b[k], 2)
	}
	if s1 == 0 || s2 == 0 {
		return 0.0, errors.New("Vectors should not be null (all zeros)")
	}
	return sumA / (math.Sqrt(s1) * math.Sqrt(s2)), nil
}
