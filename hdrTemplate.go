/*  'eucos' Euclidean Distance or Cosine Similarity using CSV files as vectors embedding DBs
    Copyright (C) 2024 gimlab

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package main

import (
	"fmt"
	"os"
	"text/template"
)

type section struct {
	Instructions string
	Examples     string
	Query        string
	Context      string
}

const (
	defaultInstructions string = "Answer QUERY using only relevant information from CONTEXT. No notes/comments."
)

const templateContext = `INSTRUCTIONS:

{{.Instructions}}

---

QUERY:

{{.Query}}

---

CONTEXT:

{{.Context}}`

var Section *section = &section{
	Instructions: defaultInstructions,
}

func createContext(f *os.File) error {
	t, err := template.New("Context Sections").Parse(templateContext)
	if err != nil {
		return fmt.Errorf("error creating context template: %w", err)
	}

	err = t.Execute(f, Section)
	if err != nil {
		return fmt.Errorf("error executing context template: %w", err)
	}

	return nil
}
