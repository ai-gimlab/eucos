# eucos

## Table of Contents

- [CHANGELOG](https://gitlab.com/ai-gimlab/eucos/-/blob/main/CHANGELOG.md#changelog)
- [Introduction](#introduction)
- [Disclaimer](#disclaimer)
- [Getting started](#getting-started)
  - [Binary](#binary)
  - [Compile from source](#compile-from-source)
  - [Default settings](#default-settings)
- [Usage example](#usage-example)
  - [Cosine Similarity](#cosine-similarity)
  - [Euclidean Distance](#euclidean-distance)
  - [Report](#report)
  - [Save context](#save-context)
    - [Text output length](#text-output-length)
  - [Colorized output](#colorized-output)
  - [More options](#more-options)
- [Credits](#credits)

---

## Introduction

eucos: terminal utility to compute Euclidean Distance or Cosine Similarity using CSV files as vectors embedding DBs.

*eucos* is written in [Go](https://go.dev/).

*eucos* query a CSV vector database to gain a quick insights of vectors quality. **Note**: it is **not intended as a production tool or as a replacement for a real vector database** ([here a list of real vector DBs](https://en.wikipedia.org/wiki/Vector_database#List_of_vector_databases)).

CSV tables must have columns `id, text, embedding`.

Can be a companion tool for the following LLMs/embedding terminal clients:

- [**geminicli**](https://gitlab.com/ai-gimlab/geminicli#geminicli---overview): Google Gemini models family
- [**gptcli**](https://gitlab.com/ai-gimlab/gptcli#gptcli---overview): OpenAI LLMs
- [**hfembed**](https://gitlab.com/ai-gimlab/hfembed#hfembed): Hugging Face embedding models
- [**mistralcli**](https://gitlab.com/ai-gimlab/mistralcli#mistralcli---overview): Mistral AI LLMs

The man page can be found [here](https://gitlab.com/ai-gimlab/eucos/-/tree/main/man).

In this guide we use the Bash shell on Linux systems, but *eucos* can run on other platforms as well.

To follow this guide a basic knowledge of what [embeddings](https://huggingface.co/blog/getting-started-with-embeddings) are is required.

---

[top](#table-of-contents)

---

## Disclaimer

I enjoy to develop this utilities just for my personal use. So, use them at your own risk.

---

[top](#table-of-contents)

---

## Getting started

### Binary

Prebuilt binary packages for Linux, Windows and MacOS can be downloaded from [here](https://gitlab.com/ai-gimlab/eucos/-/releases).

---

### Compile from source

If you prefer, clone this repo and compile from sources.

Prerequisite: [Go Development Environment](https://go.dev/dl/) installed.

Clone this repo and build:

```bash
git clone https://gitlab.com/ai-gimlab/eucos.git
cd eucos
go mod init eucos && go mod tidy
go build .
```

---

### Default settings

- **mode**: cosine similarity
- **top results**: 5
- **sort**: descending (from most to least relevant)
- **query row id**: last row of csv
- **color**: false
- **number of result chars**: full text length
- **report**: false

---

[top](#table-of-contents)

---

## Usage Example

Basically, an input CSV file with columns `id, text, embedding` is enough:

```bash
eucos --csv-db CSV_DB_EMBEDDING_FILE
```

By default a query is executed using the last row id of CSV file as "user query/prompt" and the other rows as "vectors db".

A different CSV_QUERIES_EMBEDDING_FILE file, containing "user query/prompt", can be specified using `--csv-queries CSV_QUERIES_EMBEDDING_FILE` option.

---

Let's try some basic examples.

### Cosine Similarity

By default *eucos* compute Cosine Similarity. Option `-e, --euclidean` switch to Euclidean Distance.

For all the examples below we use a couple of CSV files, *db.csv* and *queries.csv*.

File *db.csv* contains the embedding representations of the following sentences:

1. *Change is impossible, claimed the ancient Greek philosopher Parmenides*
2. *Saturn's ring system is categorized into several main groups*
3. *the term Galilean relativity refer to a somewhat modernized version of Galileo's principle*

File *queries.csv* contains the embedding representations of the following questions:

1. *How many main groups make up Saturn's ring system?*
2. *In physics, what is meant by Galilean relativity?*
3. *What did Parmenides say about change?*

We use the embedding representations of these questions to check the similarity against the sentences of *db.csv* file.

Both files *db.csv* and *queries.csv* contains the three columns `id, text, embedding`, as in the example below.

**db.csv**:

```csv
id,text,embedding
0,Change is impossible, claimed the ancient Greek philosopher Parmenides,"[-0.07617933303117752,0.031626757234334946,-0.03810399770736694,...,0.012911058031022549]"
1,Saturn's ring system is categorized into several main groups,"[0.0010612517362460494,-0.07356439530849457,-0.023778744041919708,...,0.057449884712696075]"
2,the term Galilean relativity refer to a somewhat modernized version of Galileo's principle,"[-0.04365162551403046,-0.0730254054069519,-0.029702361673116684,...,0.04416215792298317]"
```

**queries.csv**:

```csv
id,text,embedding
0,How many main groups make up Saturn's ring system?,"[-0.0030259129125624895,-0.0781233087182045,-0.051743458956480026,...,0.05540364980697632]"
1,In physics, what is meant by Galilean relativity?,"[-0.015032283030450344,-0.14954693615436554,-0.01659010536968708,...,-0.24845945835113525]"
2,What did Parmenides say about change?,"[-0.047354668378829956,0.06507744640111923,0.012506067752838135,...,0.0309610515832901]"
```

From file *queries.csv* we use the question *In physics, what is meant by Galilean relativity?*. The **row id** of this question is **1**. We pass this id as argument to option `-i, --id NUM`:

```bash
# query: COSINE SIMILARITY, id question 1
eucos --csv-db db.csv --csv-queries queries.csv --id 1
```

Results:

```text
[QUERY]
In physics, what is meant by Galilean relativity?

[COSINE SIMILARITY]
0.8067257097162469      the term Galilean relativity refer to a somewhat modernized version of Galileo's principle
0.1999786975683229      Change is impossible, claimed the ancient Greek philosopher Parmenides
0.0555098265994062      Saturn's ring system is categorized into several main groups
```

Embedding of sentence *the term Galilean relativity refer to a somewhat modernized version of Galileo's principle* has the **highest** (closest to **1**) *Cosine Similarity* value, **0.8067257097162469**, or it has the highest semantic similarity, with the embedding of question *In physics, what is meant by Galilean relativity?*, as expected.

---

The same query as example above, but this time we add `--top 1` option to **view only the top result**:

```bash
# query: COSINE SIMILARITY, id question 1, top result
eucos --csv-db db.csv --csv-queries queries.csv --id 1 --top 1
```

Results:

```text
[QUERY]
In physics, what is meant by Galilean relativity?

[COSINE SIMILARITY]
0.8067257097162469      the term Galilean relativity refer to a somewhat modernized version of Galileo's principle
```

---

The same query as examples above, but this time we add `--reverse` option to **sort results from least to most relevant**:

```bash
# query: COSINE SIMILARITY, id question 1, results reverse sorted
eucos --csv-db db.csv --csv-queries queries.csv --id 1 --reverse
```

Results:

```text
[QUERY]
In physics, what is meant by Galilean relativity?

[COSINE SIMILARITY]
0.0555098265994062      Saturn's ring system is categorized into several main groups
0.1999786975683229      Change is impossible, claimed the ancient Greek philosopher Parmenides
0.8067257097162469      the term Galilean relativity refer to a somewhat modernized version of Galileo's principle
```

---

The same query as examples above, but this time we **change question** to *How many main groups make up Saturn's ring system?*. The **row id** of this question is **0**. We pass this id as argument to option `-i, --id NUM`:

```bash
# query: COSINE SIMILARITY, id question 0
eucos --csv-db db.csv --csv-queries queries.csv --id 0
```

Results:

```text
[QUERY]
How many main groups make up Saturn's ring system?

[COSINE SIMILARITY]
0.9266263541644814      Saturn's ring system is categorized into several main groups
0.16086139571641156     Change is impossible, claimed the ancient Greek philosopher Parmenides
0.1088741674718944      the term Galilean relativity refer to a somewhat modernized version of Galileo's principle
```

Embedding of sentence *Saturn's ring system is categorized into several main groups* has the **highest** (closest to **1**) *Cosine Similarity* value, **0.9266263541644814**, or it has the highest semantic similarity, with the embedding of question *How many main groups make up Saturn's ring system?*, as expected.

---

[top](#table-of-contents)

---

### Euclidean Distance

With option `-e, --euclidean` *eucos* compute the Euclidean Distance.

Same example as above with question *How many main groups make up Saturn's ring system?*, row id **0**:

```bash
# query: EUCLIDEAN DISTANCE, id question 0
eucos --csv-db db.csv --csv-queries queries.csv --id 0 --euclidean
```

Results:

```text
[QUERY]
How many main groups make up Saturn's ring system?

[EUCLIDEAN DISTANCE]
0.38307610472426373     Saturn's ring system is categorized into several main groups
1.2954833963332137      Change is impossible, claimed the ancient Greek philosopher Parmenides
1.335010037914545       the term Galilean relativity refer to a somewhat modernized version of Galileo's principle
```

Embedding of sentence *Saturn's ring system is categorized into several main groups* has the **lowest** (closest to **0**) *Euclidean Distance* value, **0.38307610472426373**, with the embedding of question *How many main groups make up Saturn's ring system?*, as expected.

---

[top](#table-of-contents)

---

### Report

Option `--report` is meant to give an idea about the running time for a given number of rows, for a given length of the embedding vector or anything else you can think of.

Same example as above with question *How many main groups make up Saturn's ring system?*, row id **0**:

```bash
# query: EUCLIDEAN DISTANCE, id question 0, print report
eucos --csv-db db.csv --csv-queries queries.csv --id 0 --euclidean --report
```

Results:

```text
[QUERY]
How many main groups make up Saturn's ring system?

[EUCLIDEAN DISTANCE]
0.38307610472426373     Saturn's ring system is categorized into several main groups
1.2954833963332137      Change is impossible, claimed the ancient Greek philosopher Parmenides
1.335010037914545       the term Galilean relativity refer to a somewhat modernized version of Galileo's principle

[REPORT]
data vector shape:       [3, 384]
total running time:      0.002240073 sec.
loading csv data:        0.000791750 sec.
vectorize csv data:      0.001146208 sec.
query vector data:       0.000010584 sec.
sorting results:         0.000004430 sec.
```

---

[top](#table-of-contents)

---

### Save context

Results can be saved in a **context file** using `-s, --save-context FILENAME` option. The content of FILENAME can then be used as context for LLMs.

For this example let's use a small AI generated knowledge base about some Italian scientists ([here the original kb file](https://gitlab.com/ai-gimlab/eucos/-/blob/main/kb/kb.txt?ref_type=heads)). Here an excerpt:

```text
Laura Bassi
Laura Bassi, born in 1711 in Bologna, Italy, was a trailblazing figure in the 18th century,
defying societal norms to become a prominent scientist and academic. She earned a doctorate
in philosophy at the University of Bologna in 1732, distinguishing herself as the first woman
to receive a university degree. Her academic prowess led to her appointment as a professor
of physics at the university, a rare feat for a woman in that era.
.
.
Luigi Galvani
Luigi Galvani, an 18th-century Italian physician and physicist, played a pivotal role in
the history of science and medicine. Born in Bologna in 1737, Galvani's life unfolded against
the backdrop of the Enlightenment, a period marked by intellectual curiosity and scientific advancements.
.
.
etc...
```

**Note**: the text that concerns each individual scientist from the above knowledge base represents a single embedding vector.

Here some questions about the above kb:

1. *What were the first experiments that disproved spontaneous generation?*
2. *In what ways did Giovanni Aldini's experiments with "animal electricity" influence the scientific community and public perception of electricity?*
3. *How did the academic and scientific environment of Bologna in the 18th and 19th centuries foster groundbreaking discoveries and innovations?*
4. *What role did the city and the University of Bologna play in the history of wireless communication?*
5. *What were the key differences between Laura Bassi's and Luigi Galvani's approaches to electricity, and how did their work complement each other?*

**Note**: the text of each single question from the questions above represents a single embedding vector.

As usual embeddings of kb content are in file **db.csv**, embeddings of questions are in file **queries.csv**.

**db.csv**:

```csv
id,text,embedding
0,"Laura Bassi
Laura Bassi, born in 1711 in Bologna, Italy, etc...","[-0.041534654796123505,-0.0010940119391307235,-0.018826695159077644,...,-0.12370578199625015]"
1,"Luigi Galvani
Luigi Galvani, an 18th-century Italian physician etc...","[-0.07076513022184372,0.030032645910978317,0.030029816552996635,...,-0.023550665006041527]"
2,"Guglielmo Marconi
Guglielmo Marconi, an Italian inventor etc...","[-0.09706348180770874,0.05654073879122734,-0.021502571180462837,...,-0.03169223293662071]"
.
.
etc...
```

**queries.csv**:

```csv
id,text,embedding
0,What were the first experiments that disproved spontaneous generation?,"[-0.1189427450299263,0.028185537084937096,-0.003139732638373971,...,-0.010078194551169872]"
1,In what ways did Giovanni Aldini's experiments with "animal electricity" influence the scientific community and public perception of electricity?,"[-0.06833519041538239,0.0951789915561676,0.03816871717572212,...,-0.017518755048513412]"
2,How did the academic and scientific environment of Bologna in the 18th and 19th centuries foster groundbreaking discoveries and innovations?,"[0.0019024702487513423,0.025163190439343452,0.023365642875432968,...,-0.06613584607839584]"
.
.
etc...
```

The question is *What role did the city and the University of Bologna play in the history of wireless communication?*, row id **3**, context file `context.txt`, mode **Cosine Similarity**:

```bash
# query: COSINE SIMILARITY, id question 3, save retrieved 'text' to FILENAME, three closest result
eucos --csv-db db.csv --csv-queries queries.csv --id 3 --top 3 --save-context context.txt
```

Results:

```text
[QUERY]

What role did the city and the University of Bologna play in the history of wireless communication?

[COSINE SIMILARITY]
0.5643050094634761      Guglielmo Marconi
Guglielmo Marconi, an Italian inventor and electrical engineer born in 1874, revolutionized communication with his groundbreaking work on wireless telegraphy. In the late 19th and early 20th centuries, Marconi's life was marked by innovation and scientific achievements. He conducted his pioneering experiments in Bologna, Italy, where he developed the first practical and long-distance wireless communication system.
Marconi's notable accomplishment came in 1901 when he successfully transmitted the first transatlantic radio signal, covering a distance of over 2,100 miles from Poldhu in Cornwall, England, to Signal Hill in St. John's, Newfoundland. This achievement marked a pivotal moment in the history of communication, laying the foundation for modern wireless technology.
His relentless pursuit of knowledge and application of electromagnetic principles earned Marconi the Nobel Prize in Physics in 1909, making him the first person to receive the prestigious award for contributions to wireless telegraphy. Throughout his life, Marconi continued to advance wireless communication, contributing to the development of radar during World War II.
Marconi's legacy lives on in the evolution of telecommunications, as his inventions paved the way for the wireless communication systems that are integral to our interconnected world today. His scientific and academic achievements solidify his place as a pioneer in the field of telecommunications.
0.3306885340979035      Laura Bassi
Laura Bassi, born in 1711 in Bologna, Italy, was a trailblazing figure in the 18th century, defying societal norms to become a prominent scientist and academic. She earned a doctorate in philosophy at the University of Bologna in 1732, distinguishing herself as the first woman to receive a university degree. Her academic prowess led to her appointment as a professor of physics at the university, a rare feat for a woman in that era.
Bassi's home in Bologna became a hub of intellectual activity, where scholars, scientists, and philosophers gathered for stimulating discussions. Her groundbreaking work in physics, particularly in the field of electricity, garnered widespread recognition. She conducted experiments, authored numerous papers, and corresponded with prominent European scientists, solidifying her reputation as a leading figure in the scientific community.
Despite facing gender-based discrimination, Bassi's tenacity prevailed, and in 1776, she became the first female professor at the Academy of Sciences in Bologna. Her legacy extends beyond her own achievements, as she paved the way for future generations of women in science. Laura Bassi's life is a testament to the power of intellect and determination, transcending societal barriers to leave an indelible mark on the world of academia and scientific inquiry.
0.3034161549607162      Giovanni Aldini
Imagine 18th century Bologna, buzzing with intellectual curiosity. This was the lifeblood of Giovanni Aldini, born in 1762. Immersed in science from a young age, he thrived under the shadow (and tutelage) of his uncle, the famed Luigi Galvani.
Following Galvani's footsteps, Aldini dove headfirst into the mysteries of "animal electricity." He spent years dissecting animal corpses, attaching metal plates, and jolting them with voltaic batteries. Gruesome, yes, but groundbreaking. In London, he even famously stimulated the facial muscles of a recently executed criminal, creating a macabre yet mesmerizing spectacle.
But Aldini wasn't just a shock artist. He was a professor at his alma mater, the University of Bologna, sharing his knowledge with eager students. He delved into lighthouse design, fire safety measures, and even applied electricity to treat mental disorders, foreshadowing future therapies.
Beyond academia, he penned scientific works in multiple languages, bridging geographical and linguistic barriers. Recognized for his contributions, he received accolades from the Austrian emperor and left a legacy of innovation in his hometown, funding a school for artisans to explore the wonders of science.
So, Giovanni Aldini's life wasn't just about jolting dead things. It was about pushing the boundaries of scientific understanding, sharing knowledge, and leaving a lasting impact on his city and the world. In 200 words, that's just a glimpse, but hopefully, it sparks your curiosity to learn more about this fascinating figure.
```

#### Text output length

The above output is difficult to read. We can reduce the length of the output with option `-l, --length NUM_CHARS`:

```bash
# query: COSINE SIMILARITY, id question 3, save retrieved 'text' to FILENAME, three closest result, 100 chars of text
eucos --csv-db db.csv --csv-queries queries.csv --id 3 --top 3 --save-context context.txt --length 100
```

Results:

```text
[QUERY]

What role did the city and the University of Bologna play in the history of wireless communication?

[COSINE SIMILARITY]
0.5643050094634761      Guglielmo Marconi
Guglielmo Marconi, an Italian inventor and electrical engineer born in 1874,...+1375 more
0.3306885340979035      Laura Bassi
Laura Bassi, born in 1711 in Bologna, Italy, was a trailblazing figure in the 18th...+1253 more
0.3034161549607162      Giovanni Aldini
Imagine 18th century Bologna, buzzing with intellectual curiosity. This was the...+1469 more
```

The content of file `context.txt` now is:

```text
QUERY:

What role did the city and the University of Bologna play in the history of wireless 
communication?

---

CONTEXT:

- Guglielmo Marconi
Guglielmo Marconi, an Italian inventor and electrical engineer born in 1874, revolutionized 
communication with his groundbreaking work on wireless telegraphy. In the late 19th 
and early 20th centuries, Marconi's life was marked by innovation and scientific 
achievements. He conducted his pioneering experiments in Bologna, Italy, where he 
developed the first practical and long-distance wireless communication system.
Marconi's notable accomplishment came in 1901 when he successfully transmitted the 
first transatlantic radio signal, covering a distance of over 2,100 miles from Poldhu 
in Cornwall, England, to Signal Hill in St. John's, Newfoundland. This achievement 
marked a pivotal moment in the history of communication, laying the foundation for 
modern wireless technology.
His relentless pursuit of knowledge and application of electromagnetic principles 
earned Marconi the Nobel Prize in Physics in 1909, making him the first person to 
receive the prestigious award for contributions to wireless telegraphy. Throughout 
his life, Marconi continued to advance wireless communication, contributing to the 
development of radar during World War II.
Marconi's legacy lives on in the evolution of telecommunications, as his inventions 
paved the way for the wireless communication systems that are integral to our interconnected 
world today. His scientific and academic achievements solidify his place as a pioneer 
in the field of telecommunications.

- Laura Bassi
Laura Bassi, born in 1711 in Bologna, Italy, was a trailblazing figure in the 18th 
century, defying societal norms to become a prominent scientist and academic. She 
earned a doctorate in philosophy at the University of Bologna in 1732, distinguishing 
herself as the first woman to receive a university degree. Her academic prowess led 
to her appointment as a professor of physics at the university, a rare feat for a 
woman in that era.
Bassi's home in Bologna became a hub of intellectual activity, where scholars, scientists, 
and philosophers gathered for stimulating discussions. Her groundbreaking work in 
physics, particularly in the field of electricity, garnered widespread recognition. 
She conducted experiments, authored numerous papers, and corresponded with prominent 
European scientists, solidifying her reputation as a leading figure in the scientific 
community.
Despite facing gender-based discrimination, Bassi's tenacity prevailed, and in 1776, 
she became the first female professor at the Academy of Sciences in Bologna. Her 
legacy extends beyond her own achievements, as she paved the way for future generations 
of women in science. Laura Bassi's life is a testament to the power of intellect 
and determination, transcending societal barriers to leave an indelible mark on the 
world of academia and scientific inquiry.

- Giovanni Aldini
Imagine 18th century Bologna, buzzing with intellectual curiosity. This was the lifeblood 
of Giovanni Aldini, born in 1762. Immersed in science from a young age, he thrived 
under the shadow (and tutelage) of his uncle, the famed Luigi Galvani.
Following Galvani's footsteps, Aldini dove headfirst into the mysteries of "animal 
electricity." He spent years dissecting animal corpses, attaching metal plates, and 
jolting them with voltaic batteries. Gruesome, yes, but groundbreaking. In London, 
he even famously stimulated the facial muscles of a recently executed criminal, creating 
a macabre yet mesmerizing spectacle.
But Aldini wasn't just a shock artist. He was a professor at his alma mater, the 
University of Bologna, sharing his knowledge with eager students. He delved into 
lighthouse design, fire safety measures, and even applied electricity to treat mental 
disorders, foreshadowing future therapies.
Beyond academia, he penned scientific works in multiple languages, bridging geographical 
and linguistic barriers. Recognized for his contributions, he received accolades 
from the Austrian emperor and left a legacy of innovation in his hometown, funding 
a school for artisans to explore the wonders of science.
So, Giovanni Aldini's life wasn't just about jolting dead things. It was about pushing 
the boundaries of scientific understanding, sharing knowledge, and leaving a lasting 
impact on his city and the world. In 200 words, that's just a glimpse, but hopefully, 
it sparks your curiosity to learn more about this fascinating figure.
```

For this example we manually add a generic system/instruction message to the above context, such as *Extract relevant information from CONTEXT and answer QUERY using only CONTEXT. No notes/comments.*:

```text
INSTRUCTIONS:

Extract relevant information from CONTEXT and answer QUERY using only CONTEXT. No notes/comments.

---

QUERY:

What role did the city and the University of Bologna play in the history of wireless 
communication?

---

CONTEXT:

- Guglielmo Marconi
Guglielmo Marconi, an Italian inventor and electrical engineer born in 1874, revolutionized 
communication with his groundbreaking work on wireless telegraphy. In the late 19th
.
.
etc...
```

Now we query an LLM using the above context as prompt.

**Below some examples of responses from various LLMs, each with their own style**:

---

**------ RESPONSE 1 ------**

```text
The city of Bologna and the University of Bologna played a pivotal role in the history of wireless
communication through the groundbreaking work of Guglielmo Marconi. Marconi conducted his pioneering
experiments in Bologna, where he developed the first practical and long-distance wireless communication system.
His notable accomplishment came in 1901 when he successfully transmitted the first transatlantic radio signal
from Poldhu in Cornwall, England, to Signal Hill in St. John's, Newfoundland. This achievement marked
a pivotal moment in the history of communication, laying the foundation for modern wireless technology.
```

---

---

**------ RESPONSE 2 ------**

**Bologna**
- Played a pivotal role in the history of wireless communication through the contributions of Guglielmo Marconi.
- Marconi chose Bologna as the location for his pioneering experiments, where he successfully developed and demonstrated the first practical and long-distance wireless communication system.

**University of Bologna**
- Served as Marconi's base for research and experimentation in the field of wireless communication.
- Fostered Marconi's scientific endeavors, providing him with resources and an intellectual environment conducive to innovation.
- Marconi's work at the University of Bologna laid the groundwork for modern wireless technology and significantly contributed to the advancements in telecommunications.

---

---

**------ RESPONSE 3 ------**

```text
The city of Bologna and the University of Bologna both played significant roles in the history
of wireless communication. Guglielmo Marconi, an Italian inventor and electrical engineer,
conducted his pioneering experiments in Bologna, where he developed the first practical and
long-distance wireless communication system. He successfully transmitted the first transatlantic
radio signal in 1901, marking a pivotal moment in the history of communication.
Marconi's work was carried out at the University of Bologna, where he earned a degree in physics
in 1895. Additionally, Laura Bassi, a renowned scientist and professor at the University of Bologna
in the 18th century, conducted groundbreaking work in physics, particularly in the field of electricity,
which contributed to the foundation of wireless communication. Bologna's intellectual environment
and the University of Bologna's academic resources fostered an environment conducive to
groundbreaking research and innovation in the field of wireless communication.
```

---

[top](#table-of-contents)

---

### Colorized output

Using option `-c, --color` we can print results with colors:

---

---

![color](images/color.png)

---

---

![color reverse sort](images/colorReverse.png)

---

---

[top](#table-of-contents)

---

### More options

Use `--help` to view all options:

```bash
eucos --help
```

Output:

```text
Usage: eucos --csv-db CSV_DB_EMBEDDING_FILE [--csv-queries CSV_QUERIES_EMBEDDING_FILE] [OPTIONS...]

Compute Euclidean Distance or Cosine Similarity using CSV files as DBs

Defaults:
  mode: cosine similarity
  top results: 5
  sort: descending (from most to least relevant)
  query row id: last row of csv
  color: false
  number of result chars: full text length
  report: false

Notes:
  . CSV_DB_EMBEDDING_FILE is mandatory
  . cvs tables, both db and queries, must have columns 'id', 'text', 'embedding'
  . by default a query is the last row id of CSV_DB_EMBEDDING_FILE
  . to change query file use '--csv-queries CSV_QUERIES_EMBEDDING_FILE' option
  . query row id can be set with option '--id NUM' (default: last row id)
  . column 'text' from query results can be saved with option '--save-context FILENAME'
    - for testing purpose context FILENAME can be used as context for LLMs
    - if FILENAME already exists it is overwritten

Online Documentation:
  <https://gitlab.com/ai-gimlab/eucos#eucos>

OPTIONS:
  -a, --all                       return all result rows
  -c, --color                     enable output colors
  -d, --csv-db=FILENAME           db embeddings file CSV_DB_EMBEDDING_FILE (mandatory)
  -e, --euclidean                 change mode to euclidean distance
  -i, --id=NUM                    NUM: query row id (default: last row id)
  -l, --length=NUM_CHARS          how many characters of the results to print
  -o, --omit-query                omit query from output
  -q, --csv-queries=FILENAME      queries embedding file CSV_QUERIES_EMBEDDING_FILE
  -r, --reverse                   sort results in ascending order (from least to most relevant)
      --report                    print data shape and execution time
  -s, --save-context=FILENAME     save retrieved 'text' to FILENAME
                                  content of FILENAME can be used as context for LLMs
                                  if FILENAME already exists it is overwritten
  -t, --top=NUM                   return top NUM result rows (default: top 5)
      --help                      print this help
      --version                   output version information and exit

  💡                              eucos --csv-db db-embed.csv --csv-queries query-embed.csv --top 3
```

[top](#table-of-contents)

---

## Credits

This project is made possible thanks to the use of the following libraries and the precious work of those who create and maintain them.

- [getopt](https://github.com/pborman/getopt/)
- [The Algorithms - Go](https://github.com/dougwatson/Go/)

---

[top](#table-of-contents)

---

[Others gimlab repos](https://gitlab.com/users/gimaldi/groups)

---
