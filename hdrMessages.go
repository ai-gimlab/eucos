/*  'eucos' Euclidean Distance or Cosine Similarity using CSV files as vectors embedding DBs
    Copyright (C) 2024 gimlab

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package main

const helpMsgUsage = "Usage: %s --csv-db CSV_DB_EMBEDDING_FILE [--csv-queries CSV_QUERIES_EMBEDDING_FILE] [OPTIONS...]\n"

const helpMsgBody = `
Compute Euclidean Distance or Cosine Similarity using CSV files as DBs

Defaults:
  mode: cosine similarity
  top results: 5
  sort: descending (from most to least relevant)
  query row id: last row of csv
  color: false
  number of result chars: full text length
  report: false

Notes:
  . CSV_DB_EMBEDDING_FILE is mandatory
  . cvs tables, both db and queries, must have columns 'id', 'text', 'embedding'
  . by default a query is the last row id of CSV_DB_EMBEDDING_FILE
  . to change query file use '--csv-queries CSV_QUERIES_EMBEDDING_FILE' option
  . query row id can be set with option '--id NUM' (default: last row id)
  . column 'text' from query results can be saved with option '--save-context FILENAME'
    - for testing purpose context FILENAME can be used as context for LLMs
    - if FILENAME already exists it is overwritten

Online Documentation:
  <https://gitlab.com/ai-gimlab/eucos#eucos>

OPTIONS:
  -a, --all                       return all result rows
  -c, --color                     enable output colors
  -d, --csv-db=FILENAME           db embeddings file CSV_DB_EMBEDDING_FILE (mandatory)
  -e, --euclidean                 change mode to euclidean distance
  -i, --id=NUM                    NUM: query row id (default: last row id)
  -l, --length=NUM_CHARS          how many characters of the results to print
  -o, --omit-query                omit query from output
  -q, --csv-queries=FILENAME      queries embedding file CSV_QUERIES_EMBEDDING_FILE
  -r, --reverse                   sort results in ascending order (from least to most relevant)
      --report                    print data shape and execution time
  -s, --save-context=FILENAME     save retrieved 'text' to FILENAME
                                  content of FILENAME can be used as context for LLMs
				  if FILENAME already exists it is overwritten
  -t, --top=NUM                   return top NUM result rows (default: top 5)
      --help                      print this help
      --version                   output version information and exit
`

const helpMsgTips = "  💡                              %s --csv-db db-embed.csv --csv-queries query-embed.csv --top 3\n"

// befor this var, always print cmd name, without newline char (os.Args[0]). eg: fmt.Fprintf(os.Stdout, "%s", os.Args[0]); fmt.Println(verMsg)
const verMsg = ` (gimlab) 1.0.0
Copyright (C) 2024 gimlab.
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.`
