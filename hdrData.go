/*  'eucos' Euclidean Distance or Cosine Similarity using CSV files as vectors embedding DBs
    Copyright (C) 2024 gimlab

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package main

import (
	"gitlab.com/sysUtils/go-libraries/libhelp"
	"gitlab.com/sysUtils/go-libraries/libopt"
	"gitlab.com/sysUtils/go-libraries/libterm"
)

const (
	// text colors
	cyanLight         string = libterm.EscSeq + libterm.AttributeNormal + ";" + libterm.ForeLightCyan + "m"
	blueLight         string = libterm.EscSeq + libterm.AttributeNormal + ";" + libterm.ForeLightBlue + "m"
	greenLightReverse string = libterm.EscSeq + libterm.AttributeReverse + ";" + libterm.ForeLightGreen + "m"
	redLightReverse   string = libterm.EscSeq + libterm.AttributeReverse + ";" + libterm.ForeLightRed + "m"
	normal            string = libterm.Normal

	// permitted cmdline chars
	chrLowCase  string = libopt.ChrLowCase  // a, b, c, d, ...
	chrUpCase   string = libopt.ChrUpCase   // A, B, C, D, ...
	chrNums     string = libopt.ChrNums     // 0, 1, 2, 3, ...
	chrNumsPfix string = libopt.ChrNumsPfix // hexadecimal and binary number prefix (eg: 0x or 0b)
	chrFlags    string = libopt.ChrFlags    // used in cmdline options (eg: -a -b -c) - NOTE: include space
	chrCmdName  string = libopt.ChrCmdName  // used for command name (eg: ./mycmd)
	chrExtra    string = libopt.ChrExtra    // for special cases (eg: email.name@email.address)

	top               int64  = 5
	rowid             int    = -1
	headQuery         string = "QUERY"
	headDataEuclidean string = "EUCLIDEAN DISTANCE"
	headDataCosine    string = "COSINE SIMILARITY"
	headReport        string = "REPORT"
)

var (
	// vars from custom libraries
	cmdName   string = libhelp.CmdName
	tryMsg    string = libhelp.TryMsg
	logPrefix string = libhelp.LogPrefix

	topResults int64 = top
	queryRowId int   = rowid
)
