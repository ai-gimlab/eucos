/*  'eucos' Euclidean Distance or Cosine Similarity using CSV files as vectors embedding DBs
    Copyright (C) 2024 gimlab

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"

	"gitlab.com/sysUtils/go-libraries/libhelp"
	"gitlab.com/sysUtils/go-libraries/libopt"

	"github.com/dougwatson/Go/v3/math/geometry"
	"github.com/pborman/getopt/v2"
)

func main() {
	startProgram := time.Now()

	var (
		logger                                *log.Logger = log.New(os.Stderr, logPrefix, log.Lshortfile)
		hq, hd, ht, floatColor, floatTopScore string      = headQuery, "", headReport, normal, normal
		fileSaveContext                       strings.Builder
	)

	// --- check user input chars --- //
	// command name
	if err := libopt.ChkChars(chrLowCase+chrUpCase+chrNums+chrCmdName, os.Args[:1]); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}

	// check short/long options for single/double dash
	if len(os.Args) > 1 && os.Args[1] != "" && os.Args[len(os.Args)-1] != "" {
		if err := checkLongOptionDashes(os.Args[1:]); err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
	}

	// --- check cmdline options --- //
	// any error during parse
	if err := getopt.Getopt(nil); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}

	// check if string arguments was provided
	if err := argProvided(); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}

	// informative flags
	switch {
	case flagHelp:
		if len(os.Args) > 2 {
			fmt.Printf("'--%s' requested, ignoring other options. Press ENTER to continue... ", flagHelpLong)
			if _, err := fmt.Scanln(); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Println()
		}
		libhelp.PrintHelp(helpMsgUsage, helpMsgBody, helpMsgTips, true)
		os.Exit(0)
	case flagVersion:
		if len(os.Args) > 2 {
			fmt.Printf("'--%s' requested, ignoring other options. Press ENTER to continue... ", flagVersionLong)
			if _, err := fmt.Scanln(); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Println()
		}
		libhelp.PrintVersion(verMsg)
		os.Exit(0)
	}

	// check for non-option arguments
	nonOptArgs := getopt.Args()
	if len(nonOptArgs) > 0 {
		var s string
		for i, v := range nonOptArgs {
			if i == 0 {
				s += fmt.Sprintf("[OPTION %d]\n", i+1)
				s += fmt.Sprintf("%s <- PROBABLY THE SOURCE OF THE PROBLEM\n\n", v)
				continue
			} else {
				s += fmt.Sprintf("[OPTION %d]\n", i+1)
				s += fmt.Sprintf("%s\n\n", v)
			}
		}
		logger.Fatal(fmt.Errorf("\a%s:\n\n%s\n%s", "unknown option/options", s, tryMsg))
	}

	// check for mandatory flags
	if err := checkFlagMandatory(); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}

	// check format of 'string' type flags
	if err := checkFlagTypeString(); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}

	// --- config program --- //
	var fileDB string = flagDb
	var fileQuery string = flagQueryEmbedding
	var headData string = headDataCosine
	if flagEuclidean {
		headData = headDataEuclidean
	}
	hd = headData
	if fileQuery == "" {
		fileQuery = fileDB
	}
	if getopt.GetCount(flagTopLong) > 0 || getopt.GetCount(flagTopShort) > 0 {
		topResults = flagTop
	}
	if getopt.GetCount(flagIdLong) > 0 || getopt.GetCount(flagIdShort) > 0 {
		queryRowId = flagId
	}
	if flagColor {
		hq = fmt.Sprintf("%s%s%s", blueLight, headQuery, normal)
		hd = fmt.Sprintf("%s%s%s", blueLight, hd, normal)
		ht = fmt.Sprintf("%s%s%s", blueLight, headReport, normal)
		floatColor = cyanLight
		floatTopScore = greenLightReverse
		if flagReverse {
			floatTopScore = redLightReverse
		}
	}

	// --- exec --- //
	// load csv file files
	startLoadCSV := time.Now()
	db, err := os.Open(fileDB)
	if err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}
	defer db.Close()
	query, err := os.Open(fileQuery)
	if err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}
	defer query.Close()

	// read csv files content
	csvReaderDB := csv.NewReader(db)
	csvDataDB, err := csvReaderDB.ReadAll()
	if err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}
	csvReaderQuery := csv.NewReader(query)
	csvDataQuery, err := csvReaderQuery.ReadAll()
	if err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}

	// remove header row (first row) from csv
	recordsDB := csvDataDB[1:]
	recordsQuery := csvDataQuery[1:]
	endLoadCSV := time.Now()
	elapsedLoadCSV := endLoadCSV.Sub(startLoadCSV)

	// select query
	if queryRowId >= len(recordsQuery) || queryRowId < 0 {
		queryRowId = len(recordsQuery) - 1
	}
	recordQuery := recordsQuery[queryRowId : queryRowId+1]

	// vectorize data
	startVectorize := time.Now()
	vectors, err := stringToVector(recordsDB)
	if err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}
	referenceVector, err := stringToVector(recordQuery)
	if err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}
	endVectorize := time.Now()
	elapsedVectorize := endVectorize.Sub(startVectorize)

	// query
	var result map[float64]string = make(map[float64]string, len(vectors))
	var similarities []float64 = make([]float64, len(vectors), len(vectors))
	startQuery := time.Now()
	switch {
	case flagEuclidean:
		for i, v := range vectors {
			sim, err := geometry.EuclideanDistance(v, referenceVector[0])
			if err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			result[sim] = recordsDB[i][1]
			similarities[i] = sim
		}
	default:
		for i, v := range vectors {
			sim, err := Cosine(v, referenceVector[0])
			if err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			result[sim] = recordsDB[i][1]
			similarities[i] = sim
		}
	}
	endQuery := time.Now()
	elapsedQuery := endQuery.Sub(startQuery)

	// sort similarities
	startSort := time.Now()
	switch {
	case (flagEuclidean && !flagReverse) || (flagReverse && !flagEuclidean):
		sort.SliceStable(similarities, func(i, j int) bool { return similarities[i] < similarities[j] })
	case (flagEuclidean && flagReverse) || !flagEuclidean:
		sort.SliceStable(similarities, func(i, j int) bool { return similarities[i] > similarities[j] })
	}
	endSort := time.Now()
	elapsedSort := endSort.Sub(startSort)

	// print query
	if !flagOmitQuery {
		fmt.Printf("[%s]\n", hq)
		fmt.Printf("%s\n\n", recordQuery[0][1])
	}

	// store query for context template
	Section.Query = recordQuery[0][1]

	// print data
	fmt.Printf("[%s]\n", hd)
	if (len(similarities) < int(topResults)) || flagAll {
		topResults = int64(len(similarities))
	}
	for i, v := range similarities[0:int(topResults)] {
		var text string = result[v]
		lenPrint := len(result[v])
		if (getopt.GetCount(flagLengthLong) > 0 || getopt.GetCount(flagLengthShort) > 0) && flagLength < len(result[v]) {
			lenPrint = flagLength
			t := result[v][:lenPrint]
			text = t
			if lastSpaceIdx := strings.LastIndex(t, " "); lastSpaceIdx != -1 {
				text = t[:lastSpaceIdx]
			}
			text = fmt.Sprintf("%s...+%d more", text, len(result[v])-len(text))
		}
		if v == 0 || v == 1 {
			// print to stdout - add precision, because if v == 0 OR 1 print only integer part
			if i == 0 {
				// if colorized output, highlight top result
				fmt.Printf("%s%.15f%s\t%s%s\n", floatColor, v, floatTopScore, text, normal)
				continue
			}
			fmt.Printf("%s%.15f%s\t%s\n", floatColor, v, normal, text)
			continue
		}
		// convert float to printable string with full precision
		s := strconv.FormatFloat(v, 'f', -1, 64)
		trimmed := strings.TrimRight(strings.TrimRight(s, "0"), ".")
		// print to stdout
		if i == 0 {
			// if colorized output, highlight top result
			fmt.Printf("%s%s%s\t%s%s\n", floatColor, trimmed, floatTopScore, text, normal)
		} else {
			fmt.Printf("%s%s%s\t%s\n", floatColor, trimmed, normal, text)
		}
		// store data for context template
		if _, err := fileSaveContext.WriteString(fmt.Sprintf("%s\n\n", text)); err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
	}

	// write context to file
	Section.Context = fileSaveContext.String()
	if flagSaveContext != "" {
		f, err := os.Create(flagSaveContext)
		if err != nil {
			logger.Fatal(fmt.Errorf("\aerror creating context file: %v\n%s", err, tryMsg))
		}
		defer f.Close()
		if err := createContext(f); err != nil {
			logger.Fatal(fmt.Errorf("\aerror saving context file: %v\n%s", err, tryMsg))
		}
	}

	endProgram := time.Now()
	elapsedProgram := endProgram.Sub(startProgram)

	// report
	if flagReport {
		var vectorsNum int = len(vectors)
		var vectorSize int = len(vectors[0])
		fmt.Println()
		fmt.Printf("[%s]\n", ht)
		fmt.Printf("%-25s[%d, %d]\n", "data vector shape:", vectorsNum, vectorSize)
		fmt.Printf("%-25s%.9f sec.\n", "total running time:", float64(elapsedProgram)/1000000000)
		fmt.Printf("%-25s%.9f sec.\n", "loading csv data:", float64(elapsedLoadCSV)/1000000000)
		fmt.Printf("%-25s%.9f sec.\n", "vectorize csv data:", float64(elapsedVectorize)/1000000000)
		fmt.Printf("%-25s%.9f sec.\n", "query vector data:", float64(elapsedQuery)/1000000000)
		fmt.Printf("%-25s%.9f sec.\n", "sorting results:", float64(elapsedSort)/1000000000)
	}
}
