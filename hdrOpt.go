/*  'eucos' Euclidean Distance or Cosine Similarity using CSV files as vectors embedding DBs
    Copyright (C) 2024 gimlab

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package main

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/pborman/getopt/v2"
)

// how many flags are used
const flagsCount int = 14

// flag labels
const (
	fakeRune                rune   = '#'
	flagHelpShort           rune   = fakeRune
	flagHelpLong            string = "help"
	flagVersionShort        rune   = fakeRune
	flagVersionLong         string = "version"
	flagTopShort            rune   = 't'
	flagTopLong             string = "top"
	flagDbShort             rune   = 'd'
	flagDbLong              string = "csv-db"
	flagQueryEmbeddingShort rune   = 'q'
	flagQueryEmbeddingLong  string = "csv-queries"
	flagIdShort             rune   = 'i'
	flagIdLong              string = "id"
	flagReverseShort        rune   = 'r'
	flagReverseLong         string = "reverse"
	flagAllShort            rune   = 'a'
	flagAllLong             string = "all"
	flagEuclideanShort      rune   = 'e'
	flagEuclideanLong       string = "euclidean"
	flagReportShort         rune   = fakeRune
	flagReportLong          string = "report"
	flagColorShort          rune   = 'c'
	flagColorLong           string = "color"
	flagSaveContextShort    rune   = 's'
	flagSaveContextLong     string = "save-context"
	flagLengthShort         rune   = 'l'
	flagLengthLong          string = "length"
	flagOmitQueryShort      rune   = 'o'
	flagOmitQueryLong       string = "omit-query"
)

// Declare flags and have getopt return pointers to the values.
var (
	flagHelp           bool
	flagVersion        bool
	flagTop            int64
	flagDb             string
	flagQueryEmbedding string
	flagId             int
	flagReverse        bool
	flagAll            bool
	flagEuclidean      bool
	flagReport         bool
	flagColor          bool
	flagSaveContext    string
	flagLength         int
	flagOmitQuery      bool
	flags              []getopt.Option = make([]getopt.Option, 0, flagsCount)
	flagsCounter       map[string]int  = make(map[string]int)
	flagsTypes         []string        = make([]string, 0, flagsCount)
)

// init vars at program start with 'init()' internal function
func init() {
	flags = append(flags, getopt.FlagLong(&flagHelp, "help", '\U0001f595', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagHelp))
	flags = append(flags, getopt.FlagLong(&flagVersion, "version", '\U0001f6bd', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagVersion))
	flags = append(flags, getopt.FlagLong(&flagTop, flagTopLong, flagTopShort, "").SetGroup("numResult"))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagTop))
	flags = append(flags, getopt.FlagLong(&flagDb, flagDbLong, flagDbShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagDb))
	flags = append(flags, getopt.FlagLong(&flagQueryEmbedding, flagQueryEmbeddingLong, flagQueryEmbeddingShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagQueryEmbedding))
	flags = append(flags, getopt.FlagLong(&flagId, flagIdLong, flagIdShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagId))
	flags = append(flags, getopt.FlagLong(&flagReverse, flagReverseLong, flagReverseShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagReverse))
	flags = append(flags, getopt.FlagLong(&flagAll, flagAllLong, flagAllShort, "").SetGroup("numResult"))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagAll))
	flags = append(flags, getopt.FlagLong(&flagEuclidean, flagEuclideanLong, flagEuclideanShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagEuclidean))
	flags = append(flags, getopt.FlagLong(&flagReport, flagReportLong, '\U0001f000', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagReport))
	flags = append(flags, getopt.FlagLong(&flagColor, flagColorLong, flagColorShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagAll))
	flags = append(flags, getopt.FlagLong(&flagSaveContext, flagSaveContextLong, flagSaveContextShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagSaveContext))
	flags = append(flags, getopt.FlagLong(&flagLength, flagLengthLong, flagLengthShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagLength))
	flags = append(flags, getopt.FlagLong(&flagOmitQuery, flagOmitQueryLong, flagOmitQueryShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagOmitQuery))
}

// check for mandatory flags
func checkFlagMandatory() error {
	if flagDb == "" {
		return fmt.Errorf("\aoption '--%s' is mandatory", flagDbLong)
	}
	return nil
}

// check if any string was provided to string options
func argProvided() error {
	for i := range flags {
		if flags[i].Seen() && !flags[i].IsFlag() && flags[i].Value().String() == "" {
			return fmt.Errorf("option '--%s' missing or wrong argument", flags[i].LongName())
		}
	}
	return nil
}

// check if long option are given with single or double dash
// without this check long option '--abcd', written with single dash, '-abcd', is parsed as a short option '-a'
// try with 'ls' command: 'ls -l -a' and 'ls -l --author' vs 'ls -l -author' (the latter behave as 'ls -l -a')
func checkLongOptionDashes(args []string) error {
	const dash byte = '-'
	const doubleDash string = "--"
	var wasBool bool = true

	// check if arg begin with dash (if begin with dash could be an option)
loopAllGivenArgs:
	for i := 0; i < len(args); i++ {
		var counter int = 0

		// check for empty strings, such as failed shell expansion - eg: "$(cat non-existant-file)"
		if args[i] == "" {
			continue
		}

		// if previous option was not bool, then next arg is (maybe) only the option argument
		if !wasBool {
			wasBool = true
			continue
		}

		// don't check arguments without dash prefix
		if len(args[i]) > 0 && args[i][0] != dash {
			continue
		}

		// don't check arguments with dash prefix + single char - eg: -a, -b, -c, etc...
		if len(args[i]) == 2 && args[i][0] == dash {
			continue
		}

		// remove single or double dash prefix
		s := strings.TrimPrefix(args[i], string(dash))
		s = strings.TrimPrefix(s, string(dash))

		// remove first '=' char, if any (eg. --my-opt=someData)
		s, _, _ = strings.Cut(s, "=")

		// check if string is a number
		if isNum(s) {
			continue
		}

		for _, v := range flags {
			if v.LongName() == s && (args[i][0] == dash && args[i][1] != dash) {
				// if 'longoption' exist, but was given without double dash (eg: --longoption OK, -longoption WRONG)
				return fmt.Errorf("long option with single dash: '-%s' must be '--%[1]s'", s)
			} else if v.LongName() == s {
				// if option is not boolean, then next arg is not an option
				if !v.IsFlag() {
					wasBool = false
				}
				// 'longoption' exist and was given correctly (eg: --longoption)
				continue loopAllGivenArgs
			} else {
				// 'longoption' not found in this inner loop cycle
				counter++
			}
		}

		// option not found
		if counter >= len(flags) {
			return fmt.Errorf("wrong option: '%s'", args[i])
		}
	}
	return nil
}

// check 'string type' option arguments for "-" char as first char of argument
func checkFlagTypeString() error {
	for i := range flags {
		if flags[i].Seen() && (flagsTypes[i] == "string" && strings.HasPrefix(flags[i].Value().String(), "-")) {
			return fmt.Errorf("\awrong parameter for %s: '%s'", flags[i].Name(), flags[i].Value().String())
		}
	}
	return nil
}

// check if string is a number, integer or float - false = NOT number, true = IS number
func isNum(s string) bool {
	if _, err := strconv.ParseInt(s, 10, 64); err == nil {
		return true
	}
	if _, err := strconv.ParseFloat(s, 64); err == nil {
		return true
	}
	if _, err := strconv.ParseComplex(s, 128); err == nil {
		return true
	}
	return false
}
